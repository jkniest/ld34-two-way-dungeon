using UnityEngine;
using System.Collections;

public class BossAnim : StateMachineBehaviour
{
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        PlayerController.Instance.BossAnimEnd();
    }
}
