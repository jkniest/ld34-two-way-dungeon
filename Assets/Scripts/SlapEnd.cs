using UnityEngine;
using System.Collections;

public class SlapEnd : StateMachineBehaviour
{
	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
	    Enemy.attackingEnemy.FinishedAttack();
        
        
	}
}
