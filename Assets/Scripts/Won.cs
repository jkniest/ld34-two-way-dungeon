using UnityEngine;

public class Won : MonoBehaviour
{
    #region PUBLIC_MEMBER_VAR

    public GameObject exitButton;

    #endregion
    
    public void ExitGame()
    {
        Application.Quit();
    }
    
    private void Awake()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer) exitButton.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetButtonDown("Button B")) ExitGame();
    }
}
