using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Boss : Singleton<Boss>
{
    #region PUBLIC_MEMBER_VAR

    public int healthPoints { get; protected set; }
    public int damage { get; protected set; }

    public bool potatoFlying;

    public GameObject potatoPrefab;
    public Transform shootPos;

    public Potato potato;

    #endregion

    #region PRIVATE_METHODS

    private void Awake()
    {
        healthPoints = 130;
        damage = 24;
    }

    private void Update()
    {
        if (PlayerController.Instance.moving) return;
             
        if (potato != null && potato.flyBack && Vector3.Distance(potato.transform.position, transform.position) <= 1f)
        {
            PlayerController.Instance.AddLog("Boss got " + potato.damage + " damage! (HP left: " + healthPoints + ")");

            healthPoints -= potato.damage;
            if (healthPoints <= 0)
            {
                SceneManager.LoadScene("__Done", LoadSceneMode.Single);
            }

            potatoFlying = false;
            Destroy(potato.gameObject);
        }

        if (!potatoFlying)
        {
            var go = Instantiate(potatoPrefab, shootPos.position, shootPos.rotation) as GameObject;
            go.GetComponent<Potato>().startPos = shootPos.position;
            potatoFlying = true;

            PlayerController.Instance.potato = go.transform;
            potato = go.GetComponent<Potato>();
        }
    }

    #endregion
}
