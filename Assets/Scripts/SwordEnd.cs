using UnityEngine;
using System.Collections;

public class SwordEnd : StateMachineBehaviour
{
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
	    PlayerController.Instance.FinishedAttack();
	}
}
