using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection.Emit;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : Singleton<PlayerController>
{
    #region PUBLIC_MEMBER_VAR
    
    public AudioSource creepyAudioSource;
    public Animator dodgeAnimator;
    public Animator swordAnimator;

    public int damage { get; protected set; }
    public int healthPoints { get; protected set; }

    public Text logText;
    public bool playerTurn { get; protected set; }
    public AttackType attacktype { get; protected set; }

    public int swordLevel;
    public float swordPercentage { get; protected set; }

    public Color[] swordLevels;
    public Transform potato;
    public bool inBossAnimation = false;

    public  bool moving { get; private set; }

    #endregion

    #region PRIVATE_MEMBER_VAR

    private readonly List<string> logLines = new List<string>(); 

    #endregion

    #region PUBLIC_METHODS

    public void Hit(int _damagePoints)
    {
        healthPoints -= _damagePoints;
        AddLog("Got " + _damagePoints + " damage! (HP: " + healthPoints + ")");

        if (healthPoints <= 0) Die();
    }

    public void FinishedEnemyTurn()
    {
        playerTurn = true;
        AddLog("It's your turn! Press (Q) or (E)");
    }

    public void FinishedAttack()
    {
        playerTurn = false;
        AddLog("It's your enemy turn!");
        attacktype = AttackType.NONE;
    }

    public void AddLog(string _msg)
    {
        if (logLines.Count == 5) logLines.RemoveAt(0);
        logLines.Add(_msg);

        logText.text = logLines.Aggregate("", (current, line) => current + (line + "\n"));
    }

    public void KilledEnemy()
    {
        swordPercentage += 0.2f;
        if (swordPercentage >= 1f)
        {
            swordLevel++;
            swordPercentage = 0f;
        }

        damage = (swordLevel*10) + Mathf.RoundToInt((swordPercentage*10));

        if (swordLevel < swordLevels.Length)
        {
            swordAnimator.GetComponentInChildren<MeshRenderer>().materials[1].color =
                Color.Lerp(swordAnimator.GetComponentInChildren<MeshRenderer>().materials[1].color, swordLevels[swordLevel],
                    swordPercentage);
        }
    }

    public void BossAnimEnd()
    {
        inBossAnimation = false;
    }

    #endregion

    #region PRIVATE_METHODS

    private void Start()
    {
        damage = 10;
        healthPoints = 80;
        swordLevel = 1;
        swordPercentage = 0f;

        logText.text = "";
    }

    private void Die()
    {
        AddLog("You are dead!");
        SceneManager.LoadScene("__GameOver", LoadSceneMode.Single);
    }

    private void Update()
    {
        creepyAudioSource.mute = !(RoomGenerator.Instance.currentRoom.aliveEnemies.Count > 0);

        swordAnimator.SetBool("hittingLeft", false);
        swordAnimator.SetBool("hittingRight", false);

        if (RoomGenerator.Instance.currentRoom.type != RoomGenerator.RoomType.BOSS)
        {
            if (moving)
            {
                GetComponent<Animator>().SetBool("moveLeft", false);
                GetComponent<Animator>().SetBool("moveRight", false);

                if (!GetComponent<Animator>().GetBool("movingFinished")) return;

                moving = false;
                GetComponent<Animator>().SetBool("movingFinished", false);

                foreach (var room in RoomGenerator.Instance.rooms)
                {
                    room.OnMovementEnd();
                }
            }
            else
            {
                if (RoomGenerator.Instance.currentRoom.type != RoomGenerator.RoomType.BOSS)
                {
                    if (Enemy.attackingEnemy != null)
                    {
                        var enemy = Enemy.attackingEnemy;

                        if (!playerTurn)
                        {
                            switch (enemy.attackType)
                            {
                                case Enemy.AttackType.DODGED:
                                    dodgeAnimator.SetBool("dodgedRight", false);
                                    dodgeAnimator.SetBool("dodgedLeft", false);
                                    break;
                                case Enemy.AttackType.LEFT:
                                    if (Input.GetButtonDown("Button B") && !Input.GetButton("Button A"))
                                    {
                                        enemy.DodgedAttack();
                                        dodgeAnimator.SetBool("dodgedLeft", true);
                                        AddLog("Dodged enemy attack!");
                                    }
                                    break;
                                case Enemy.AttackType.RIGHT:
                                    if (Input.GetButtonDown("Button A") && !Input.GetButton("Button B"))
                                    {
                                        enemy.DodgedAttack();
                                        dodgeAnimator.SetBool("dodgedRight", true);
                                        AddLog("Dodged enemy attack!");
                                    }
                                    break;
                                case Enemy.AttackType.SPECIAL:
                                    if (Input.GetButton("Button A") && Input.GetButton("Button B"))
                                    {
                                        enemy.DodgedAttack();
                                        AddLog("Dodged enemy special-attack!");
                                    }
                                    break;
                                case Enemy.AttackType.WAITING:
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }
                        }
                        else
                        {
                            if (attacktype != AttackType.NONE) return;

                            if (Input.GetButtonDown("Button A"))
                            {
                                attacktype = AttackType.LEFT;
                                swordAnimator.SetBool("hittingLeft", true);
                            }
                            else if (Input.GetButtonDown("Button B"))
                            {
                                attacktype = AttackType.RIGHT;
                                swordAnimator.SetBool("hittingRight", true);
                            }
                        }
                    }


                    if (!RoomGenerator.Instance.currentRoom.ready ||
                        RoomGenerator.Instance.currentRoom.aliveEnemies.Count > 0)
                        return;

                    if (Input.GetButtonDown("Button A"))
                    {
                        var door = RoomGenerator.Instance.currentRoom.doorLeft;

                        RoomGenerator.Instance.AddRoom(true);
                        moving = true;

                        GetComponent<Animator>().SetBool("moveLeft", true);
                        door.GetComponent<Animator>().SetBool("open", true);
                    }
                    else if (Input.GetButtonDown("Button B"))
                    {
                        var door = RoomGenerator.Instance.currentRoom.doorRight;

                        RoomGenerator.Instance.AddRoom(false);
                        moving = true;

                        GetComponent<Animator>().SetBool("moveRight", true);
                        door.GetComponent<Animator>().SetBool("open", true);
                    }
                }
            }
        }
        else
        {
            if (moving)
            {
                GetComponent<Animator>().SetBool("moveLeft", false);
                GetComponent<Animator>().SetBool("moveRight", false);

                if (!GetComponent<Animator>().GetBool("movingFinished")) return;

                moving = false;
                GetComponent<Animator>().SetBool("movingFinished", false);

                foreach (var room in RoomGenerator.Instance.rooms)
                {
                    room.OnMovementEnd();
                }
            }
            else
            {
                dodgeAnimator.SetBool("dodgedRight", false);
                dodgeAnimator.SetBool("dodgedLeft", false);
                GetComponent<Animator>().SetBool("moveLeft", false);
                GetComponent<Animator>().SetBool("moveRight", false);
                swordAnimator.SetBool("bossAttack", false);

                if (potato == null) return;

                if (!inBossAnimation)
                {
                    if (Input.GetButton("Button A") && Input.GetButton("Button B"))
                    {
                        swordAnimator.SetBool("bossAttack", true);
                        inBossAnimation = true;
                        
                        Debug.Log(Vector3.Distance(transform.position, potato.position));
                        if (Vector3.Distance(transform.position, potato.position) <= 3f &&
                            Vector3.Distance(transform.position, potato.position) >= 0.4f)
                        {
                            potato.GetComponent<Potato>().flyBack = true;
                            potato.GetComponent<Potato>().damage = damage;
                        }
                    }
                }

                // Debug.Log(potato.position + " - " + transform.position + " => " + Vector3.Distance(transform.position, potato.position));
                if (Vector3.Distance(transform.position, potato.position) <= 0.8f && !potato.GetComponent<Potato>().flyBack)
                {
                    Hit(Boss.Instance.damage);
                    Destroy(potato.gameObject);
                    Boss.Instance.potatoFlying = false;
                }
            }
        }
    }

    #endregion

    #region ENUMS

    public enum AttackType
    {
        NONE, LEFT, RIGHT
    }

    #endregion
}
