﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    #region PUBLIC_STATIC_FIELDS

    public static T Instance
    {
        get
        {
            if(pInstance != null) return pInstance;

            var obj = Object.FindObjectOfType<T>();
            if (obj != null)
            {
                pInstance = obj;
                return pInstance;
            }

            var go = new GameObject("(Singleton) " + typeof(T));
            pInstance = go.AddComponent<T>();
            return pInstance;
        }
    }

    #endregion

    #region PRIVATE_STATIC_VAR

    private static T pInstance;

    #endregion
}
