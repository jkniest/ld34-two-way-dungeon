using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Debug = System.Diagnostics.Debug;

public class RoomGenerator : Singleton<RoomGenerator>
{
    #region PUBLIC_MEMBER_VAR

    public GameObject defaultRoomPrefab;
    public GameObject shopRoomPrefab;
    public GameObject itemRoomPrefab;
    public GameObject bossRoomPrefab;
    public GameObject trapRoomPrefab;
    public GameObject rootRoomPrefab;

    public GameObject[] enemyPool;

    public int roomSizeInUnits;

    #endregion

    #region PUBLIC_FIELDS

    [NotNull]
    public Room currentRoom
    {
        // ReSharper disable once FunctionRecursiveOnAllPaths
        get { return pCurrentRoom;  }
        private set
        {
            if (value == null) throw new ArgumentNullException();

            pCurrentRoom = value;
            rooms.Add(value);

            if (rooms.Count < 3) return;
            var room = rooms[0];
            rooms.RemoveAt(0);
            Destroy(room.gameObject);
        }
    }

    #endregion

    #region PUBLIC_MEMBER_VAR

    public readonly List<Room> rooms = new List<Room>();

    #endregion

    #region PRIVATE_MEMBER_VAR

    private Room pCurrentRoom;
    
    #endregion

    #region PUBLIC_METHODS

    public void AddRoom(bool _left)
    {
        currentRoom = GenerateRoom(currentRoom, _left, (_left) ? currentRoom.leftRoom : currentRoom.rightRoom);
    }

    #endregion

    #region PRIVATE_METHODS

    private void Awake()
    {
        currentRoom = GenerateRoom(null, false, RoomType.ROOT);
    }
    
    private Room GenerateRoom(Room _room, bool _left, RoomType _type)
    {
        var layer = (_room != null ) ? _room.layer + 1 : 0;

        if (layer > 0 && layer%10 == 0)
        {
            _type = RoomType.BOSS;
        }

        var posZ = layer*roomSizeInUnits;
        var posX = (_room == null) ? 0 : ((_left) ? -8.0f : 8.0f);

        if (_room != null) posX = _room.transform.position.x + posX;

        GameObject prefab;
        switch (_type)
        {
            case RoomType.SHOP:
                prefab = shopRoomPrefab;
                break;
            case RoomType.DEFAULT:
                prefab = defaultRoomPrefab;
                break;
            case RoomType.ITEM:
                prefab = itemRoomPrefab;
                break;
            case RoomType.BOSS:
                prefab = bossRoomPrefab;
                break;
            case RoomType.TRAP:
                prefab = trapRoomPrefab;
                break;
            case RoomType.ROOT:
                prefab = rootRoomPrefab;
                break;
            default:
                prefab = defaultRoomPrefab;
                break;
        }

        var go = GameObject.Instantiate(prefab, new Vector3(posX, 0, posZ), Quaternion.identity) as GameObject;
        go.GetComponent<Room>().layer = (_room == null) ? 0 : _room.layer + 1;
        return go.GetComponent<Room>();
    }

    #endregion

    #region ENUMS

    public enum RoomType
    {
        DEFAULT, SHOP, ITEM, BOSS, TRAP, ROOT
    }

    #endregion
}
