using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking.NetworkSystem;

public class Room : MonoBehaviour
{
    #region PUBLIC_STATIC_VAR

    public static bool firstEnemy = true;

    #endregion

    #region PUBLIC_MEMBER_VAR

    public int layer = 0;

    public GameObject doorLeft;
    public GameObject doorRight;

    public TextMesh[] textMeshes;
    public RoomGenerator.RoomType type;

    public bool ready { get; private set; }
    public readonly List<GameObject> aliveEnemies = new List<GameObject>();

    public RoomGenerator.RoomType leftRoom;
    public RoomGenerator.RoomType rightRoom;

    public GameObject bossLeft, bossRight;

    #endregion

    #region PRIVATE_MEMBER_VAR

    private int textIndex = 0;
    private bool attackOngoing = false;

    #endregion

    #region PUBLIC_METHODS

    public void OnMovementEnd()
    {
        if (firstEnemy && type == RoomGenerator.RoomType.DEFAULT)
        {
            textMeshes[0].gameObject.SetActive(true);
            firstEnemy = false;
        }
    }

    public void KilledEnemy(Enemy _enemy)
    {
        attackOngoing = false;
        aliveEnemies.Remove(_enemy.gameObject);
    }

    #endregion

    #region PRIVATE_METHODS

    private void Start()
    {
        if (layer == 0 && type == RoomGenerator.RoomType.ROOT)
            textMeshes[0].gameObject.SetActive(true);

        if (layer%4 == 0 && layer > 0 && type != RoomGenerator.RoomType.BOSS)
        {
            var left = (Random.Range(0, 100) <= 50);
            leftRoom = (left) ? RoomGenerator.RoomType.BOSS : RoomGenerator.RoomType.DEFAULT;
            rightRoom = (!left) ? RoomGenerator.RoomType.BOSS : RoomGenerator.RoomType.DEFAULT;

            bossLeft.gameObject.SetActive(left);
            bossRight.gameObject.SetActive(!left);
        }
        else
        {
            leftRoom = RoomGenerator.RoomType.DEFAULT;
            rightRoom = RoomGenerator.RoomType.DEFAULT;
        }

        if (type == RoomGenerator.RoomType.DEFAULT)
        {
            if( !firstEnemy ) textMeshes = new TextMesh[0];

            var spawnSpots = GameObject.FindGameObjectsWithTag("EnemySpot");
            foreach (var spot in spawnSpots)
            {
                if (spot.transform.parent != transform) continue;
                var enemy = RoomGenerator.Instance.enemyPool[Random.Range(0, RoomGenerator.Instance.enemyPool.Length)];

                var go = GameObject.Instantiate(enemy, spot.transform.position, spot.transform.rotation) as GameObject;
                go.transform.SetParent(transform, true);
                aliveEnemies.Add(go);
            }
        }
    }

    private void Update()
    {
        if (AtLastTextMesh()) ready = true;

        if (ready && !attackOngoing && aliveEnemies.Count > 0 )
        {
            aliveEnemies[0].GetComponent<Enemy>().StartAttack();
            attackOngoing = true;
        }
        
        if (Input.GetButtonDown("Button B"))
        {
            textIndex++;
            textIndex = Mathf.Clamp(textIndex, 0, textMeshes.Length - 1);

            for (var i = 0; i < textMeshes.Length; i++)
            {
                textMeshes[i].gameObject.SetActive((textIndex == i));
            }
        }
    }

    private bool AtLastTextMesh()
    {
        return (textIndex >= textMeshes.Length - 1);
    }

    #endregion
}
