using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    #region PUBLIC_MEMBER_VAR

    public GameObject exitButton;

    #endregion

    #region PUBLIC_METHODS

    public void PlayAgain()
    {
        
    }

    public void ExitGame()
    {
        Application.Quit();
    } 

    #endregion

    #region PRIVATE_METHODS

    private void Awake()
    {
        if( Application.platform == RuntimePlatform.WebGLPlayer ) exitButton.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetButtonDown("Button A")) PlayAgain();
        if (Input.GetButtonDown("Button B")) ExitGame();
    }

    #endregion
}
