using System;
using UnityEngine;
using System.Collections;
using System.Security.Permissions;

public class Enemy : MonoBehaviour
{
    #region PUBLIC_STATIC_VAR

    public static Enemy attackingEnemy;

    #endregion

    #region PUBLIC_MEMBER_VAR

    public int healthPoints;
    public int damage = 10;
    public float speed = 10f;

    public float attackTimer = 4f;
    public Animator animator;

    public AttackType attackType { get; protected set; }
    public bool canDodge { get; protected set; }

    #endregion

    #region PRIVATE_MEMBER_VAR

    private Vector3 targetPosition;
    private Quaternion targetRotation;

    private bool attacking = false;
    private bool finishedMovement = false;
    private bool dodged = false;
    private bool alreadyHitted = false;

    private float timer;
    private readonly System.Random random = new System.Random();

    #endregion

    #region PUBLIC_METHODS

    public void StartAttack()
    {
        var moveTargets = GameObject.FindGameObjectsWithTag("MoveTarget");
        var moveTarget = moveTargets[0];

        // ReSharper disable once LoopCanBePartlyConvertedToQuery
        foreach (var target in moveTargets)
        {
            if (target.transform.parent != transform.parent) continue;
            moveTarget = target;
            break;
        }

        targetPosition = moveTarget.transform.position;
        targetRotation = moveTarget.transform.rotation;
        attacking = true;
        attackingEnemy = this;
    }

    public void FinishedAttack()
    {
        if (dodged)
        {
            dodged = false;

            if (healthPoints <= 0) Die();
            PlayerController.Instance.FinishedEnemyTurn();
            return;
        }

        PlayerController.Instance.FinishedEnemyTurn();
        PlayerController.Instance.Hit(damage);
    }

    public void DodgedAttack()
    {
        attackType = AttackType.DODGED;
        dodged = true;
    }

    #endregion

    #region PRIVATE_METHODS

    private void Update()
    {
        if (!attacking) return;

        if (!finishedMovement)
        {
            transform.position = Vector3.Lerp(transform.position, targetPosition, speed*Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, speed*Time.deltaTime);
        }

        if (Vector3.Distance(transform.position, targetPosition) <= 0.01f)
            finishedMovement = true;

        if (!finishedMovement) return;

        animator.SetBool("specialAttack", false);
        animator.SetBool("slapLeft", false);
        animator.SetBool("slapRight", false);
        animator.SetBool("dodgedLeft", false);

        if (!PlayerController.Instance.playerTurn)
        {
            alreadyHitted = false;
            if (timer > 0) timer -= Time.deltaTime;
            else
            {
                timer = attackTimer;
                var hitLeft = (random.Next(2) == 0);
                
                if (random.Next(1, 100) <= 3)
                {
                    Debug.Log("SPEZIAL HIT!!!");

                    animator.SetBool("specialAttack", true);
                    animator.SetBool("slapLeft", false);
                    animator.SetBool("slapRight", false);

                    attackType = AttackType.SPECIAL;

                    return;
                }

                animator.SetBool("slapLeft", hitLeft);
                animator.SetBool("slapRight", !hitLeft);
                animator.SetFloat("animSpeed", (float)(random.NextDouble() * (2 - 0.5) + 0.5f));

                attackType = hitLeft ? AttackType.LEFT : AttackType.RIGHT;
            }
        }
        else
        {
            var dodgeLeft = (random.Next(2) == 0);

            if (alreadyHitted) return;

            switch (PlayerController.Instance.attacktype)
            {
                case PlayerController.AttackType.NONE:
                    // Do nothing
                    break;
                case PlayerController.AttackType.LEFT:
                    if (dodgeLeft)
                    {
                        PlayerController.Instance.AddLog("Enemy dodged!");
                        animator.SetBool("dodgedLeft", true);
                    }
                    else
                    {
                        var dmg = PlayerController.Instance.damage;
                        healthPoints -= dmg;
                        PlayerController.Instance.AddLog("Hitted enemy with " + dmg + " damage. HP left: " + healthPoints);
                        if (healthPoints <= 0)
                        {
                            PlayerController.Instance.AddLog("Enemy died!");
                            Die();
                        }
                    }
                    alreadyHitted = true;
                    break;
                case PlayerController.AttackType.RIGHT:
                    if (!dodgeLeft)
                    {
                        PlayerController.Instance.AddLog("Enemy dodged!");
                    }
                    else
                    {
                        var dmg = PlayerController.Instance.damage;
                        healthPoints -= dmg;
                        PlayerController.Instance.AddLog("Hitted enemy with " + dmg + " damage. HP left: " + healthPoints);
                        if (healthPoints <= 0)
                        {
                            PlayerController.Instance.AddLog("Enemy died!");
                            Die();
                        }
                    }
                    alreadyHitted = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    private void Die()
    {
        // TODO(Jordan): Particles
        PlayerController.Instance.KilledEnemy();
        RoomGenerator.Instance.currentRoom.KilledEnemy(this);
        Destroy(gameObject);
    }

    #endregion

    #region ENUMS

    public enum AttackType
    {
        DODGED,
        LEFT,
        RIGHT,
        SPECIAL,
        WAITING
    }

    #endregion
}
