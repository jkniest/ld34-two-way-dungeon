using UnityEngine;

public class Potato : MonoBehaviour
{
    #region PUBLIC_MEMBER_VAR

    public bool flyBack;
    public int damage = 0;

    public Vector3 startPos;

    #endregion

    #region PRIVATE_METHODS

    private void Start()
    {
        
    }

    private void Update()
    {
        if( !flyBack ) transform.Translate(0, 0, 3*Time.deltaTime);
        else transform.Translate(0, 0, -3*Time.deltaTime);
    }

    #endregion
}